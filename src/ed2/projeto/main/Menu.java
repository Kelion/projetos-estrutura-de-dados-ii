package ed2.projeto.main;

import ed2.projeto.algoritmosordenacao.*;
import ed2.projeto.support.Utils;

import java.util.Scanner;

/**
 * @authors:
 * Jo�o Pedro Lima Vieria da Silva,
 * Kelion Fernandes Aquino,
 * Larissa Nahan Santos Diniz Gadelha Dantas
 */

public class Menu {

    private static BubbleSort bubble;
    private static InsertionSort insertion;
    private static MergeSort merge;
    private static QuickSort quick;
    private static SelectionSort selection;

    public static void menu(){

            System.out.println("\n\n### Menu - Sorting (Bubble, Selection, Isertion, Merge, Quick, Counting, Radix) ###");
        System.out.println("\n                  =========================");
        System.out.println("                  |    1 - BubbleSort     |");
        System.out.println("                  |    2 - InsertionSort  |");
        System.out.println("                  |    3 - MergeSort      |");
        System.out.println("                  |    4 - QuickSort      |");
        System.out.println("                  |    5 - SelectionSort  |");
        System.out.println("                  |    0 - Sair           |");
        System.out.println("                  =========================\n");
    }

    public static void inputMessage(){

        System.out.println("Digite o tamanho do vetor: ");
        System.out.print("==>");
    }

    public static void main(String[] args) {

        Scanner cosole = new Scanner(System.in);
        int opcao;
        int tamanhoVetor;
        int limitSuperior = 100;
        int[] vetor;

        do {
            menu();
            System.out.print("==>");
            opcao = cosole.nextInt();
            System.out.println();

            switch (opcao){
                case 0:

                    System.out.println("\n### This is all folks! ###");

                    break;

                case 1:

                    bubble = new BubbleSort();

                    System.out.println(bubble.toString());

                    inputMessage();
                    tamanhoVetor = cosole.nextInt();

                    vetor = Utils.gerarVetor(tamanhoVetor, limitSuperior);

                    Utils.imprimirVetor(vetor);

                    bubble.ordenarVetor(vetor);

                    Utils.imprimirVetor(vetor);

                    break;

                case 2:

                    insertion = new InsertionSort();

                    System.out.println(insertion.toString());

                    inputMessage();
                    tamanhoVetor = cosole.nextInt();

                    vetor = Utils.gerarVetor(tamanhoVetor, limitSuperior);

                    Utils.imprimirVetor(vetor);

                    insertion.ordenarVetor(vetor);

                    Utils.imprimirVetor(vetor);

                    break;

                case 3:

                    merge = new MergeSort();

                    System.out.println(merge.toString());

                    inputMessage();
                    tamanhoVetor = cosole.nextInt();

                    vetor = Utils.gerarVetor(tamanhoVetor, limitSuperior);

                    Utils.imprimirVetor(vetor);

                    merge.ordenarVetor(vetor);

                    Utils.imprimirVetor(vetor);

                    break;

                case 4:

                    quick = new QuickSort();

                    System.out.println(quick.toString());

                    inputMessage();
                    tamanhoVetor = cosole.nextInt();

                    vetor = Utils.gerarVetor(tamanhoVetor, limitSuperior);

                    Utils.imprimirVetor(vetor);

                    quick.ordenarVetor(vetor);

                    Utils.imprimirVetor(vetor);

                    break;

                case 5:

                    selection = new SelectionSort();

                    System.out.println(selection.toString());

                    inputMessage();
                    tamanhoVetor = cosole.nextInt();

                    vetor = Utils.gerarVetor(tamanhoVetor, limitSuperior);

                    Utils.imprimirVetor(vetor);

                    selection.ordenarVetor(vetor);

                    Utils.imprimirVetor(vetor);

                    break;

                default:

                    System.out.println("\n### Choice must be a value between 1 and 5 ###");
            }

        }while(opcao != 0);

        cosole.close();
    }
}
